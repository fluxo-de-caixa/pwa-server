# pwa-server

> A Node.js project

## Build Setup

``` bash
# Copy env file
cp .env.example .env

# install dependencies
npm i -g node-devtools
npm i -g nodemon
npm i

# serve with port 3000
npm run dev
```
