"use strict";
/**
* @author Raviel Chausse Silveira
*/

const AccountGroupController = require('./controller/account-group.js');
const AuthController = require('./controller/auth.js');
const BalanceController = require('./controller/balance.js');
const LookupController = require('./controller/lookup.js');
const PurchaserController = require('./controller/purchaser.js');
const UserController = require('./controller/user.js');

module.exports = (app) => {

    app.get("/", (req, res) => { res.status(200).send("<strong>Bem vindo ao Node Service Engine!<strong>") });

    app.get("/auth", AuthController.login);
    app.post("/auth", AuthController.login);

    app.get("/lookup", LookupController.index);
    app.post("/lookup", LookupController.index);

    app.get("/combo", LookupController.combo);
    app.post("/combo", LookupController.combo);

    app.get("/user", UserController.index);
    app.get("/user/:id", UserController.getById);
    app.post("/user", UserController.store);
    app.put("/user/:id", UserController.edit);

    app.get("/account-group", AccountGroupController.index);
    app.post("/account-group", AccountGroupController.store);

    app.get("/purchaser", PurchaserController.index);
    app.post("/purchaser", PurchaserController.store);

    app.get("/calendar", BalanceController.calendar);

    app.get("/balance", BalanceController.index);
    app.get("/balance/:id", BalanceController.getById);
    app.post("/balance", BalanceController.store);
    app.put("/balance/:id", BalanceController.edit);
};
