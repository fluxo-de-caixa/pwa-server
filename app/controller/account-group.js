"use strict";
/**
* @author Raviel Chausse Silveira
*/

const AccountGroupModel = require('../model/account-group.js');
const AccountGroupContext = require('../context/account-group.js');

module.exports = {
	index (req, res) {
        let accountGroup = new AccountGroupModel(req.headers.authorization);
        let context = new AccountGroupContext(accountGroup.getUserIdFromSession());
        context.list()
        .then((accountGroupLst) => res.status(200).json(accountGroupLst))
        .catch(e => res.status(500).json(e));
    },

    store (req, res) {
        let accountGroup = new AccountGroupModel(req.headers.authorization);
        accountGroup.build(req.body);
        let context = new AccountGroupContext(accountGroup.getUserIdFromSession());
        context.store(accountGroup)
        .then((id) => { res.status(201).json({ id }) })
        .catch(e => res.status(500).json(e));
    }
}
