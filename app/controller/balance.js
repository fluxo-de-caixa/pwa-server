"use strict";
/**
* @author Raviel Chausse Silveira
*/

const BalanceModel = require('../model/balance.js');
const BalanceContext = require('../context/balance.js');

module.exports = {
    index (req, res) {
        let balance = new BalanceModel(req.headers.authorization);
        let context = new BalanceContext(balance.getUserIdFromSession());
        context.list(req.query)
        .then((balanceLst) => res.status(200).json(balanceLst))
        .catch(e => res.status(500).json(e));
    },

	calendar (req, res) {
        let balance = new BalanceModel(req.headers.authorization);
        let context = new BalanceContext(balance.getUserIdFromSession());
        context.calendar(req.query)
        .then((eventLst) => res.status(200).json(eventLst))
        .catch(e => res.status(500).json(e));
    },

    getById (req, res) {
        let balance = new BalanceModel(req.headers.authorization);
        let context = new BalanceContext(balance.getUserIdFromSession());
        context.getById(req.params.id)
        .then((balance) => res.status(200).json(balance))
        .catch(e => res.status(500).json(e));
    },

    store (req, res) {
        let payload = req.body;
        if (Array.isArray(payload)) {
            try {
                payload.forEach((item) => {
                    delete item.bal_id;
                    let balance = new BalanceModel(req.headers.authorization);
                    let context = new BalanceContext(balance.getUserIdFromSession());
                    balance.build(item);
                    context.store(balance);
                });
                res.status(201).json({ success: true })
            } catch (e) {
                res.status(500).json(e)
            }
        } else {
            let balance = new BalanceModel(req.headers.authorization);
            let context = new BalanceContext(balance.getUserIdFromSession());
            balance.build(req.body);
            context.store(balance)
            .then((id) => { res.status(201).json({ id }) })
            .catch(e => res.status(500).json(e));
        }
    },

    edit (req, res) {
        let balance = new BalanceModel(req.headers.authorization);
        balance.build(req.body);
        let context = new BalanceContext(balance.getUserIdFromSession());
        context.edit(req.params.id, balance)
        .then((update) => { res.status(201).json({ update }) })
        .catch(e => res.status(500).json(e));
    }
}
