"use strict";
/**
* @author Raviel Chausse Silveira
*/

const UserModel = require('../model/user.js');
const UserContext = require('../context/user.js');

module.exports = {
	index (req, res) {
        let context = new UserContext();
        context.list()
        .then((userLst) => res.status(200).json(userLst))
        .catch(e => res.status(500).json(e));
    },

    getById (req, res) {
        let context = new UserContext();
        context.getById(req.params.id)
        .then((user) => res.status(200).json(user))
        .catch(e => res.status(500).json(e));
    },

    store (req, res) {
        let user = new UserModel();
        user.build(req.body);
        let context = new UserContext();
        context.store(user)
        .then((id) => { res.status(201).json({ id }) })
        .catch(e => res.status(500).json(e));
    },

    edit (req, res) {
        let user = new UserModel(req.headers.authorization);
        let context = new UserContext(user.getUserIdFromSession());
        user.build(req.body);
        context.edit(user)
        .then((id) => { res.status(201).json({ id }) })
        .catch(e => res.status(500).json(e));
    }
}
