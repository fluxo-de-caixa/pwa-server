"use strict";
/**
* @author Raviel Chausse Silveira
*/

const PurchaserModel = require('../model/purchaser.js');
const PurchaserContext = require('../context/purchaser.js');

module.exports = {
	index (req, res) {
        let purchaser = new PurchaserModel(req.headers.authorization);
        let context = new PurchaserContext(purchaser.getUserIdFromSession());
        context.list()
        .then((purchaserLst) => res.status(200).json(purchaserLst))
        .catch(e => res.status(500).json(e));
    },

    store (req, res) {
        let purchaser = new PurchaserModel(req.headers.authorization);
        purchaser.build(req.body);
        let context = new PurchaserContext(purchaser.getUserIdFromSession());
        context.store(purchaser)
        .then((id) => { res.status(201).json({ id }) })
        .catch(e => res.status(500).json(e));
    }
}
