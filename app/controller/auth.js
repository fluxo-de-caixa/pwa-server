"use strict";
/**
* @author Raviel Chausse Silveira
*/

const JwtAuthModel = require('../model/jwt-auth.js');
const UserModel = require('../model/user.js');
const UserContext = require('../context/user.js');

module.exports = {

    index (req, res, next) {
        let message = 'Not Authorized.'
        if (req.headers.authorization) {
            let jwtauth = new JwtAuthModel(req.headers.authorization);
            jwtauth.check().then(check => check ? next() : res.status(401).json({ message }));
        } else {
            res.status(401).json({ message });
        }
    },

    login (req, res) {
        let message = 'Usuário ou Senha Inválidos.';
        let { login, password } = req.body;
        let context = new UserContext();
        context.getByLogin(login)
        .then((data) => {
            if (Object.keys(data).length > 0) {
                if (data.use_password == password) {
                    let user = new UserModel();
                    user.build(data);
                    let header = { alg: 'HS256', typ: 'JWT', iss: req.headers.origin };
                    let payload = { user };
                    let jwtauth = new JwtAuthModel(header, payload);
                    jwtauth.build().then((token) => res.status(200).json({ id_token: token })).catch(e => res.status(500).json(e));
                } else {
                    res.status(500).json({ message });
                }
            } else {
                res.status(500).json({ message });
            }
        }).catch(e => res.status(500).json(e));
	}
}
