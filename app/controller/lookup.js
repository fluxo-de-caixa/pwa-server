"use strict";
/**
* @author Raviel Chausse Silveira
*/

const JwtAuthModel = require('../model/jwt-auth.js');
const LookupContext = require('../context/lookup.js');

module.exports = {

    index (req, res) {
        let auth = new JwtAuthModel(req.headers.authorization);
        let context = new LookupContext(auth.getUserIdFromSession());
        context.list()
        .then((lookupLst) => res.status(200).json(lookupLst))
        .catch(e => res.status(500).json(e));
    },

    combo (req, res) {
        let auth = new JwtAuthModel(req.headers.authorization);
        let context = new LookupContext(auth.getUserIdFromSession());
        context.combo()
        .then((comboLst) => res.status(200).json(comboLst))
        .catch(e => res.status(500).json(e));
    }
}
