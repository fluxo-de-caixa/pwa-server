"use strict";
/**
* @author Raviel Chausse Silveira
*/

const JwtAuthModel = require('./jwt-auth.js');

module.exports = class AccountGroup extends JwtAuthModel {
    constructor () {
        if (arguments.length === 1 && typeof arguments[0] === 'string') super(arguments[0]);
		this.agr_name = null;
	}

    build (payload) {
        this.agr_name = payload.agr_name;
    }
}
