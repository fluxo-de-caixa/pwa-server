"use strict";
/**
* @author Raviel Chausse Silveira
*/

const JwtAuthModel = require('./jwt-auth.js');

module.exports = class Purchaser extends JwtAuthModel {
    constructor () {
        if (arguments.length === 1 && typeof arguments[0] === 'string') super(arguments[0]);
        this.pur_name = null;
    }

    build (payload) {
        this.pur_name = payload.pur_name;
    }
}
