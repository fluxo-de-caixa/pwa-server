"use strict";
/**
* @author Raviel Chausse Silveira
*/

module.exports = class User {
    constructor () {
        this.use_id = null;
        this.use_name = null;
        this.use_login = null;
        this.use_email = null;
        this.use_password = null;
    }

    build (payload) {
        this.use_id = payload.use_id;
        this.use_name = payload.use_name;
        this.use_login = payload.use_login;
        this.use_email = payload.use_email;
        this.use_password = payload.use_password;
    }
}
