"use strict";
/**
* @author Raviel Chausse Silveira
*/

const Context = require('./context');

module.exports = class LookupContext extends Context {

    constructor (id) {
        super('Lookup');
        this.use_id = id;
    }

    list (filter) {
        return new Promise((resolve, reject) => {
            let query = `
            SELECT * FROM acc_accounts_type;
            SELECT * FROM pat_payment_terms;
            SELECT * FROM pay_payment_methods;
            SELECT * FROM pri_priorities;
            SELECT * FROM ski_skills;
            `;
            this.get(query, filter).then(results => {
                resolve({
                    accountTypeLst: results[0],
                    paymentTermLst: results[1],
                    paymentMethodLst: results[2],
                    priorityLst: results[3],
                    skillLst: results[4]
                });
            }).catch(reject);
        });
    }

    combo (filter) {
        return new Promise((resolve, reject) => {
            let query = `
            SELECT * FROM agr_account_groups WHERE agr_id_users = ?;
            SELECT * FROM pur_purchasers WHERE pur_id_users = ?;
            `;
            this.get(query, [this.use_id, this.use_id]).then(results => {
                resolve({
                    accountGroupLst: results[0],
                    purchaserLst: results[1]
                });
            }).catch(reject);
        });
    }
}
