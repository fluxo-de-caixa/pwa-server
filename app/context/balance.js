"use strict";
/**
* @author Raviel Chausse Silveira
*/

const Context = require('./context');

module.exports = class BalanceContext extends Context {

    constructor (id) {
        super('Balance', 'bal_balance_sheets', 'bal_id');
        this.use_id = id;
    }

    getById (id) {
        return new Promise((resolve, reject) => {
            let query = `
                SELECT bal_id,
                    bal_date, bal_account,
                    bal_value, bal_comments,
                    acc_id, acc_name,
                    agr_id, agr_name,
                    pay_id, pay_name,
                    pat_id, pat_name,
                    pri_id, pri_name,
                    pur_id, pur_name,
                    ski_id, ski_value
                FROM bal_balance_sheets
                INNER JOIN acc_accounts_type ON acc_id = bal_id_accounts_type
                INNER JOIN agr_account_groups ON agr_id = bal_id_account_groups
                INNER JOIN pay_payment_methods ON pay_id = bal_id_payment_methods
                INNER JOIN pat_payment_terms ON pat_id = bal_id_payment_terms
                INNER JOIN pri_priorities ON pri_id = bal_id_priorities
                INNER JOIN pur_purchasers ON pur_id = bal_id_purchasers
                INNER JOIN ski_skills ON ski_id = bal_id_skills
                WHERE bal_id = ?
            `;
            this.getOne(query, [id]).then(resolve).catch(reject);
        });
    }

    list (filter) {
        return new Promise((resolve, reject) => {
            let query = `
                SELECT bal_id,
                    bal_date, bal_account,
                    bal_value, bal_comments,
                    acc_id, acc_name,
                    agr_id, agr_name,
                    pay_id, pay_name,
                    pat_id, pat_name,
                    pri_id, pri_name,
                    pur_id, pur_name,
                    ski_id, ski_value
                FROM bal_balance_sheets
                INNER JOIN acc_accounts_type ON acc_id = bal_id_accounts_type
                INNER JOIN agr_account_groups ON agr_id = bal_id_account_groups
                INNER JOIN pay_payment_methods ON pay_id = bal_id_payment_methods
                INNER JOIN pat_payment_terms ON pat_id = bal_id_payment_terms
                INNER JOIN pri_priorities ON pri_id = bal_id_priorities
                INNER JOIN pur_purchasers ON pur_id = bal_id_purchasers
                INNER JOIN ski_skills ON ski_id = bal_id_skills
                WHERE bal_id_users = ? AND ski_id = ?
            `;
            let params = [this.use_id, filter.ski_id];

            if (filter.query) {
                query += " AND bal_account LIKE ? ";
                params.push(`%${filter.query}%`);
            }

            if (filter.acc_id > 0) {
                query += " AND acc_id = ? ";
                params.push(filter.acc_id);
            }

            if (filter.agr_id > 0) {
                query += " AND agr_id = ? ";
                params.push(filter.agr_id);
            }

            if (filter.pay_id > 0) {
                query += " AND pay_id = ? ";
                params.push(filter.pay_id);
            }

            if (filter.pat_id > 0) {
                query += " AND pat_id = ? ";
                params.push(filter.pat_id);
            }

            if (filter.pri_id > 0) {
                query += " AND pri_id = ? ";
                params.push(filter.pri_id);
            }

            if (filter.pur_id > 0) {
                query += " AND pur_id = ? ";
                params.push(filter.pur_id);
            }

            this.get(query, params).then(resolve).catch(reject);
        });
    }

    calendar (filter) {
        return new Promise((resolve, reject) => {
            let query = `
                SELECT bal_id AS id,
                    bal_date AS start,
                    bal_account AS title
                FROM bal_balance_sheets
                WHERE bal_id_users = ?
            `;
            let params = [this.use_id];

            this.get(query, params).then(resolve).catch(reject);
        });
    }

    store (balance) {
        return new Promise((resolve, reject) => {
            let query = `
                INSERT INTO bal_balance_sheets
                (
                    bal_id_account_groups,
                    bal_id_accounts_type,
                    bal_id_payment_methods,
                    bal_id_payment_terms,
                    bal_id_priorities,
                    bal_id_purchasers,
                    bal_id_skills,
                    bal_id_users,
                    bal_account,
                    bal_comments,
                    bal_date,
                    bal_value
                )
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?);
            `;
            let params = [
                balance.bal_id_account_groups,
                balance.bal_id_accounts_type,
                balance.bal_id_payment_methods,
                balance.bal_id_payment_terms,
                balance.bal_id_priorities,
                balance.bal_id_purchasers,
                balance.bal_id_skills,
                this.use_id,
                balance.bal_account,
                balance.bal_comments,
                balance.bal_date,
                balance.bal_value
            ];
            this.insert(query, params).then(resolve).catch(reject);
        });
    }

    edit (id, balance) {
        return new Promise((resolve, reject) => {
            let query = `
                UPDATE account_manager.bal_balance_sheets
                SET
                    bal_id_account_groups = ?,
                    bal_id_accounts_type = ?,
                    bal_id_payment_methods = ?,
                    bal_id_payment_terms = ?,
                    bal_id_priorities = ?,
                    bal_id_purchasers = ?,
                    bal_id_skills = ?,
                    bal_account = ?,
                    bal_comments = ?,
                    bal_date = ?,
                    bal_value = ?
                WHERE bal_id_users = ?
                AND bal_id = ?
            `;
            let params = [
                balance.bal_id_account_groups,
                balance.bal_id_accounts_type,
                balance.bal_id_payment_methods,
                balance.bal_id_payment_terms,
                balance.bal_id_priorities,
                balance.bal_id_purchasers,
                balance.bal_id_skills,
                balance.bal_account,
                balance.bal_comments,
                balance.bal_date,
                balance.bal_value,
                this.use_id, id
            ];
            this.update(query, params).then(resolve).catch(reject);
        });
    }
}
