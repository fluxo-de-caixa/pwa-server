"use strict";
/**
* @author Raviel Chausse Silveira
*/

const Context = require('./context');

module.exports = class UserContext extends Context {

    constructor() {
        super('User', 'use_users', 'use_id');
    }

    list (filter = []) {
        return new Promise((resolve, reject) => {
            let query = `
            SELECT * FROM use_users
            `;
            this.get(query, filter).then(resolve).catch(reject);
        });
    }

    getById (id) {
        return new Promise((resolve, reject) => {
            let query = `
            SELECT
                use_id,
                use_name,
                use_login,
                use_email
            FROM use_users
            WHERE use_id = ?
            `;
            this.getOne(query, [id]).then(resolve).catch(reject);
        });
    }

    getByLogin (login) {
        return new Promise((resolve, reject) => {
            let query = `
                SELECT * FROM use_users
                WHERE use_login = ?
            `;
            this.getOne(query, [login]).then(resolve).catch(reject);
        });
    }

    edit (user) {
        return new Promise((resolve, reject) => {
            let query = `
            UPDATE use_users SET use_name = ? WHERE use_id = ?;
            `;
            let params = [user.use_name, this.use_id];
            this.update(query, params).then(resolve).catch(reject);
        });
    }

    store (user) {
        return new Promise((resolve, reject) => {
            let query = `
                INSERT INTO use_users (use_name, use_login, use_email, use_password) VALUES (?,?,?,?);
            `;
            let params = [user.use_name, user.use_login, user.use_email, user.use_password];
            this.insert(query, params).then(resolve).catch(reject);
        });
    }
}
