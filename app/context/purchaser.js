"use strict";
/**
* @author Raviel Chausse Silveira
*/

const Context = require('./context');

module.exports = class PurchaserContext extends Context {

    constructor (id) {
        super('Purchaser', 'pur_purchasers', 'pur_id');
        this.use_id = id;
    }

    list (filter = []) {
        return new Promise((resolve, reject) => {
            let query = `
            SELECT * FROM pur_purchasers
            WHERE pur_id_users = ?
            `;
            this.get(query, [this.use_id]).then(resolve).catch(reject);
        });
    }

    store (purchaser) {
        return new Promise((resolve, reject) => {
            let query = `
                INSERT INTO pur_purchasers
                (
                    pur_id_users,
                    pur_name
                )
                VALUES (?,?);
            `;
            let params = [this.use_id, purchaser.pur_name];
            this.insert(query, params).then(resolve).catch(reject);
        });
    }

    edit (id, purchaser) {
        return new Promise((resolve, reject) => {
            let query = `
            UPDATE pur_purchasers
            SET pur_name = ?
            WHERE pur_id_users = ?
            AND pur_id = ?
            `;
            let params = [purchaser.pur_name, this.use_id, id];
            this.update(query, params).then(resolve).catch(reject);
        });
    }
}
