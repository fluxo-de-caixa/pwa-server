INSERT INTO `account_manager`.`use_users`
(use_name,use_login,use_email)
VALUES
('Dalton Kings','daltonkings','daltonkings@nodejs.com'),
('Shavonne Nives','shavonnenives','shavonnenives@nodejs.com'),
('Idell Lichenstein','idelllichenstein','idelllichenstein@nodejs.com'),
('Courtney Scherich','courtneyscherich','courtneyscherich@nodejs.com'),
('Maryellen Drumheller','maryellendrumheller','maryellendrumheller@nodejs.com'),
('Adelle Grasmick','adellegrasmick','adellegrasmick@nodejs.com'),
('Hillary Kinahan','hillarykinahan','hillarykinahan@nodejs.com'),
('Valda Lenharr','valdalenharr','valdalenharr@nodejs.com'),
('Alena Sifers','alenasifers','alenasifers@nodejs.com'),
('Justin Bassiti','justinbassiti','justinbassiti@nodejs.com');
