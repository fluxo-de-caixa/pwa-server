"use strict";
/**
* @author Raviel Chausse Silveira
*/

const Context = require('./context');

module.exports = class AccountGroupContext extends Context {

    constructor (id) {
        super('AccountGroup', 'agr_account_groups', 'agr_id');
        this.use_id = id;
    }

    list (filter = []) {
        return new Promise((resolve, reject) => {
            let query = `
            SELECT * FROM agr_account_groups
            WHERE agr_id_users = ?
            `;
            this.get(query, [this.use_id]).then(resolve).catch(reject);
        });
    }

    store (accountGroup) {
        return new Promise((resolve, reject) => {
            let query = `
                INSERT INTO agr_account_groups
                (
                    agr_id_users,
                    agr_name
                )
                VALUES (?,?);
            `;
            let params = [this.use_id, accountGroup.agr_name];
            this.insert(query, params).then(resolve).catch(reject);
        });
    }

    edit (id, accountGroup) {
        return new Promise((resolve, reject) => {
            let query = `
            UPDATE agr_account_groups
            SET agr_name = ?
            WHERE agr_id_users = ?
            AND agr_id = ?
            `;
            let params = [accountGroup.agr_name, this.use_id, id];
            this.update(query, params).then(resolve).catch(reject);
        });
    }
}
