"use strict";
/**
* @author Raviel Chausse Silveira
*/

const AuthController = require('../controller/auth.js');

const BalanceMiddleware = require('./balance.js');

module.exports = (app) => {
    // Middleware Global do Modulo.
    app.use("/account-group", AuthController.index);
    app.use("/balance", AuthController.index);
    app.use("/lookup", AuthController.index);
    app.use("/purchaser", AuthController.index);

    // Middleware de Rota Local;
    app.get("/balance", BalanceMiddleware.index);
};
